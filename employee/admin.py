from django.contrib import admin
from employee.models import Employee

class EmployeeAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_at'

admin.site.register(Employee, EmployeeAdmin)
