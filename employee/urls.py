from django.conf.urls import url
from employee import views

urlpatterns = [
    url(r'^employee/$', views.EmployeeList.as_view()),
    url(r'^employee/(?P<pk>[0-9]+)/$', views.EmployeeDetail.as_view()),
]