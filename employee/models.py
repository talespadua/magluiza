from django.utils.timezone import now
from django.db import models
from department.models import Department

# Create your models here.
class Employee(models.Model):
    name = models.CharField('Name of employee', max_length=200)
    email = models.CharField('Name of employee', max_length=200)
    department = models.ForeignKey(Department, on_delete=models.CASCADE)
    created_at = models.DateTimeField(default=now)

    def __str__(self):
        return self.name
